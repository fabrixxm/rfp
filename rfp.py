#!/usr/bin/env python3
#
# Prints metadata from RadioFreccia web radio
# Optionally pipe stream to player via fifo
#
# Copyright 2018 Fabio Comuni <fabrix.xm@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# https://en.wikipedia.org/wiki/MPEG_transport_stream
# https://en.wikipedia.org/wiki/Packetized_elementary_stream
# http://id3.org/id3v2.4.0-structure
import os
import sys
import struct
from io import BytesIO
import json
import time
import requests
import datetime
import locale
import atexit
import argparse


from mutagen.id3 import ID3

FMT_SONG = "{present[mus_art_name]} : '{present[mus_sng_title]}' da '{present[mus_sng_itunesalbumname]}' :: {show[prg_title]} con {show[speakers]}"
FMT_COMM = "Pubblicità :: {show[prg_title]} con {show[speakers]}"

PLAYLIST="http://rtl-radio-stream.thron.com/live/radio6/radio6/playlist.m3u8"
FIFOPATH="/tmp/rfp.ts"

PACKET_WIDTH=188
PID=0x102

fifofd = None # keep fifo fd open globally

class EOFException(IOError):
    pass


class TSException(Exception):
    pass


class PESException(TSException):
    pass


class ID3Exception(Exception):
    pass


class HTTPError(Exception):
    pass


class PESPacket:
    def __init__(self, fd, pos):
        header = readValue(fd, 4)
        startcode = header >> 8
        if startcode != 0x000001:
            raise PESException("Invalid PES start code")

        self.sid = header & 0xff
        self.pklen = readValue(fd, 2)
        self.payload = b''

        optheader = readValue(fd,1)
        marker = optheader >> 6
        if marker != 0x2:
            self.payload = optheader  # no header, add to payload
        else:
            optheader2 = readValue(fd,1)
            headerlen = readValue(fd,1)
            readData(fd,headerlen) # remainder of PES header

    def add_payload(self, fd, length):
        self.payload += readData(fd,length)

    def __str__(self):
        return "PES [{}] LEN: {}".format(self.sid, self.pklen)

def _read(fd, width):
    global fifofd
    buff = fd.read(width)
    if fifofd:
        fifofd.write(buff)
    return buff

def readValue(fd, width):
    fmts={
        4:'>L',
        2:'>H',
        1:'>B'
    }

    buff = _read(fd, width)
    if not buff:
        raise EOFException
    return struct.unpack(fmts[width], buff[:width])[0]

def readData(fd, width):
    return _read(fd, width)


def parse_ts(fd):
    pos = 0
    packet_n = 0
    pespk = None
    try:
        while True:
            sync = readValue(fd, 1)

            if sync==0x47:
                header = readValue(fd, 2)
                tei = (header >> 15) & 0x1
                pusi= (header >> 14) & 0x1
                tp  = (header >> 13) & 0x1
                pid = header & 0x1FFF
                # header cont.
                header = readValue(fd, 1)
                tsc = (header >> 6) & 0x3
                adaptation_field = (header >> 4) & 0x3
                continuity_counter = header & 0xF

                if pid==PID:
                    # print("")
                    # print("{:016b}".format(header))
                    # print("{:08x}:{}[{}] TEI:{} PUSI:{} TP:{} TSC:{:02b} AF:{:02b} CC:{}".format(
                    #     pos, packet_n, pid, tei, pusi, tp, tsc, adaptation_field, continuity_counter
                    # ))

                    if adaptation_field & 0x2:
                        adaptation_field_len = readValue(fd, 1)
                        # print("adaptation_field_len:", adaptation_field_len)
                        readData(fd,adaptation_field_len) # read and forget


                    if pusi and (adaptation_field & 0x1):
                        pespk = PESPacket(fd, pos)
                        pespk.add_payload(fd, pos+PACKET_WIDTH-fd.tell())
                        # print("\t", pespk)

                    else:
                        if pespk:
                            pespk.add_payload(fd,pos+PACKET_WIDTH-fd.tell())

                packet_n+=1
                readData(fd,pos+PACKET_WIDTH-fd.tell()) # jump to next packet
                pos = pos + PACKET_WIDTH
                #fd.seek(pos)
    except EOFException:
        pass

    return pespk.payload


# def syncsafe(sfb):
#     value = 0
#     step = 0
#     while sfb > 0:
#         value += (sfb & 0xFF) << (step * 7)
#         sfb = sfb >> 8
#         step += 1
#     return value

# def get_id3(fd):
    # id3data = parse_ts(fd)
    # with open("test.id3", "wb") as f:
    #     f.write(id3data)
    # id3buff = BytesIO(id3data)
    # if id3buff.read(3) != b'ID3':
    #     raise ID3Exception("ID3 identifier missing")
    #
    # version = readValue(id3buff, 2)
    # print("ID3 version: {:04x}".format(version))
    # flags = readValue(id3buff, 1)
    # f_unsync       = flags & 0x80 == 0x80
    # f_extrahead    = flags & 0x40 == 0x40
    # f_experimental = flags & 0x20 == 0x20
    # f_hasfooter    = flags & 0x10 == 0x10
    # if (flags & 0xF) != 0:
    #     raise ID3Exception("ID3 invalid flags byte 0b{:08b}".format(flags))
    #
    # size = syncsafe(readValue(id3buff, 4))
    # print("size:", size)


def get_m3u8_urls(url, lastdate=None):
    headers = {}
    if lastdate is not None:
        headers['If-Modified-Since'] = lastdate
    r = requests.get(url, headers=headers)
    if r.status_code == 304: # Not Modified, nothing to see
        return []
    if r.status_code == requests.codes.ok:
        lines = r.text.split("\n")
        for l in lines:
            if l.strip().startswith("http"):
                yield l
    return []


def print_info(t):
    data = {}
    try:
        tit1 = t['TIT1']
        jsons = "".join(tit1.text).strip(" -")
        if jsons=="":
            return
        data = json.loads(jsons)
    except KeyError:
        return
    except Exception as e:
        print ("[E] {}".format(e))

    try:
        if 'present' not in data['songInfo']:
            return

        if data['songInfo']['present']['class'] == "Commercial":
            print(FMT_COMM.format(**data['songInfo']))

        else:
            print(FMT_SONG.format(**data['songInfo']))

    except KeyError as e:
        print ("[E] {}".format(e))
        import pprint
        pprint.pprint(data)


def get_current_date_as_http_header():
    dt = datetime.datetime.utcnow()
    locale.setlocale(locale.LC_ALL, 'C')
    str = dt.strftime('%a, %d %b %Y %H:%M:%S GMT')
    locale.setlocale(locale.LC_ALL, '')
    return str


def main(once=True):
    chunklist_url = list(get_m3u8_urls(PLAYLIST))[0]

    lastdata = b''
    lastdate = None

    while True:
        chunks_urls = get_m3u8_urls(chunklist_url, lastdate)
        for cu in chunks_urls:
            lastdate = get_current_date_as_http_header()
            r = requests.get(cu, stream=True)
            try:
                id3data = parse_ts(r.raw)
                if id3data != lastdata:
                    lastdata = id3data
                    tags = ID3(BytesIO(id3data))
                    print_info(tags)
            except TSException as e:
                print(e)
        if once and lastdata!=b'':
            break
        time.sleep(4)


def cleanup():
    if fifofd is not None:
        fifofd.close()
        # TODO: remove fifo file


if __name__=="__main__":
    atexit.register(cleanup)

    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--fifo', help='Write stream data to fifo', action="store_true")
    parser.add_argument('-c', '--current', help='Print current song info', action="store_true", default=False)
    args = parser.parse_args()

    if args.fifo:
        if not os.path.exists(FIFOPATH):
            os.mkfifo(FIFOPATH)
        print("Open {} with your favourite music player.".format(FIFOPATH))
        fifofd = open(FIFOPATH, "wb")

    main(once=args.current)
